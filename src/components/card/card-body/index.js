import React from 'react'

import { Flex } from '../../containers'

const CardBody=({children,...props})=>{
    return(
        <Flex {...props}>
            {children}
        </Flex>
    )
}

export default CardBody;