import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components';

const StyledInput=styled.input`
    border:0;
    outline:0;
    border-bottom:1px solid #aaa;
`

const Input = ({
    type,
    placeholder,
    value, 
    onChange
}) => (
    <StyledInput 
        data-testid="basic-input"
        type={type}
        placeholder={placeholder} 
        value={value} 
        onChange={onChange}
    />
)

Input.propTypes={
    type:PropTypes.string,
    placeholder:PropTypes.string,
    value:PropTypes.string,
    onChange:PropTypes.func
}

Input.defaultProps={
    type:'text',
    placeholder:'',
    value:''
}

export default Input