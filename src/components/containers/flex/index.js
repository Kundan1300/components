import React from 'react';
import styled from 'styled-components';

const StyledFlex=styled.div`
    display:flex;
    flex-direction:${props=>props.direction?props.direction:'column'};
    margin-top:${props=>props.mt?props.mt+"rem":'0'};
    margin-bottom:${props=>props.mb?props.mb+"rem":'0'};
    margin-left:${props=>props.ml?props.ml+"rem":'0'};
    margin-right:${props=>props.mr?props.mr+"rem":'0'};
    padding-top:${props=>props.pt?props.pt+"rem":'0'};
    padding-bottom:${props=>props.pb?props.pb+"rem":'0'};
    padding-left:${props=>props.pl?props.pl+"rem":'0'};
    padding-right:${props=>props.pr?props.pr+"rem":'0'};
`;

const Flex=({children,...props})=>(
    <StyledFlex {...props}>
        {children}
    </StyledFlex>
)
export default Flex;