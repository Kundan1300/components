import React from 'react';
import styled from 'styled-components';

import Flex from '../flex'

const StyledFlex=styled(Flex)`
    background-color:${props=>props.bgColor?props.bgColor:'white'}
`;

const CardContainer=({children,...props})=>{
    return(
        <StyledFlex {...props}>
            {children}
        </StyledFlex>
    )
}

export default CardContainer